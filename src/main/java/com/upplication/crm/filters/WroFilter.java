package com.upplication.crm.filters;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import ro.isdc.wro.http.ConfigurableWroFilter;

import javax.servlet.Filter;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.util.Properties;

@WebFilter(urlPatterns = "/resources/css/*")
public class WroFilter extends ConfigurableWroFilter implements Filter {

    private final Properties props;

    public WroFilter() throws IOException {
        super();
        this.props = PropertiesLoaderUtils.loadProperties(new EncodedResource(new ClassPathResource("/wro/wro.properties")));
        this.setProperties(props);
    }
}
