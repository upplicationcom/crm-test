package com.upplication.crm.market.requests.s3;

import com.upplication.s3fs.S3Path;
import es.upplication.exception.FileException;
import es.upplication.file.util.AmazonS3Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;

@Component
public class AwsS3Service {

    @Autowired
    private AmazonS3Util amazonS3Util;
    @Value("${upplication.aws.bucket}")
    private String s3BucketName;

    /**
     * Upload file image to s3 bucket and set it to public
     * The file is saved in the resource market folder for the childupp id
     * passed by parameter
     *
     * @param uploadFile Path path with the new file to upload
     * @param childUppId int, the id to save the file in the childupps folder
     * @param fileName File name to use at the final file uploaded
     * @return new public S3Path from amazon s3 with the new file uploaded
     */
    public S3Path uploadResource(Path uploadFile, int childUppId, String fileName) throws IOException {
        String key = "resources/" + childUppId + "/market/" + Instant.now().getEpochSecond() + "_" + fileName;
        return upload(uploadFile, key);
    }


    /**
     * Upload file image to s3 bucket and set it to public
     * The file is saved in the pwa folder for the childupp id
     * passed by parameter.
     *
     * Also upload the icon with the epoch second in the same directory
     * so you can look up the historic.
     *
     * @param uploadFile Path path with the new file to upload
     * @param childUppId int, the id to save the file in the childupps folder
     * @param fileName File name to use at the final file uploaded
     * @return new public Url from amazon s3 with the new file uploaded
     */
    public String uploadPWA(Path uploadFile, int childUppId, String fileName) throws IOException {
        String base = "resources/" + childUppId + "/pwa/";
        upload(uploadFile,  base + Instant.now().getEpochSecond() + "_" + fileName);
        Path path = upload(uploadFile,  base + fileName);
        return amazonS3Util.getPublicURL(path).toString();
    }

    private S3Path upload(Path uploadFile, String key) throws IOException {
        try (InputStream stream = Files.newInputStream(uploadFile)) {
            return (S3Path) amazonS3Util.upload(stream, URI.create("s3://s3-eu-west-1.amazonaws.com/" + s3BucketName + "/" + key));
        }
        catch (FileException e) {
            throw new IOException(e);
        }
    }
}
