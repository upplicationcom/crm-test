package com.upplication.crm.market.requests;

import com.upplication.crm.market.requests.domain.*;
import com.upplication.crm.support.web.PageWrapper;
import es.upplication.entities.type.ScreenshotDeviceType;
import es.upplication.entities.type.StatusPublishRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@Controller
@RequestMapping("market")
public class RequestsController {

    Logger logger = LoggerFactory.getLogger(RequestsController.class);

    private final RequestsManager requestsManager;

    public RequestsController(final RequestsManager requestsManager) {
        this.requestsManager = requestsManager;
    }

    private static final String REQUESTS_VIEW = "market/requests/index";

    @RequestMapping(value = "requests", method = RequestMethod.GET)
    public String requests(Model model,
                           @PageableDefault(sort = {"requestDate"}, direction = Sort.Direction.DESC, value = 20) Pageable pageable,
                           @RequestParam(value = "childUppId", required = false, defaultValue = "-1") int childUppId) {
        PageWrapper<Request> page =
                new PageWrapper<> (requestsManager.getMarketRequests(pageable, childUppId), "requests");

        model.addAttribute("page", page);
        model.addAttribute("childUppId", childUppId);
        return REQUESTS_VIEW;
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public PublishRequestResponse save(@Valid @ModelAttribute PublishRequestForm form) {
        logger.info("Saving publish request form for Request ID " + form.getRequestId());
        return requestsManager.savePublication(form);
    }

    @RequestMapping(value = "publish-ios", method = RequestMethod.POST)
    @ResponseBody
    public PublishRequestResponse publishIos(@Valid @ModelAttribute PublishRequestForm form) {
        logger.info("Publish iOS request form for Request ID " + form.getRequestId());
        return requestsManager.createIosApp(form);
    }

    @RequestMapping(value = "publish-android", method = RequestMethod.POST)
    @ResponseBody
    public PublishRequestResponse publishAndroid(@Valid @ModelAttribute PublishRequestForm form) {
        logger.info("Create Android request form for Request ID " + form.getRequestId());
        return requestsManager.createAndroidApp(form);
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public void delete(@RequestParam(value="requestId") int requestId) {
        requestsManager.deletePublication(requestId);
    }

    @RequestMapping(value = "delete-screenshot", method = RequestMethod.POST)
    @ResponseBody
    public ResourceResponse deleteScreenshot(@RequestParam(value="requestId") int requestId, @RequestParam(value="position") int position,
                                             @RequestParam(value="device") ScreenshotDeviceType device) {
        requestsManager.deleteScreenshot(requestId, position, device);
        ResourceResponse viewResponse = new ResourceResponse();
        viewResponse.setId(requestId);
        viewResponse.setPosition(position);
        viewResponse.setDevice(device);
        return viewResponse;
    }

    @RequestMapping(value = "upload-icon", method = RequestMethod.POST)
    @ResponseBody
    public ResourceResponse uploadIcon(@RequestParam(value="requestId") int requestId, @RequestParam(value="icon") MultipartFile icon) {

        if (icon.isEmpty()) {
            return null;
        }

        String url = requestsManager.uploadIcon(requestId, icon);
        ResourceResponse response = new ResourceResponse();
        response.setId(requestId);
        response.setUrl(url);
        return response;
    }

    @RequestMapping(value = "upload-screenshot", method = RequestMethod.POST)
    @ResponseBody
    public ResourceResponse uploadScreenshot(@Valid @ModelAttribute ScreenshotForm screenshot) {
        // deleting possible previous screenshot

        if (screenshot.getFile().isEmpty()) {
            return null;
        }

        requestsManager.deleteScreenshot(screenshot.getRequestId(), screenshot.getPosition(), screenshot.getDevice());
        String url = requestsManager.uploadScreenshot(screenshot);
        ResourceResponse resourceResponse = new ResourceResponse();
        resourceResponse.setId(screenshot.getRequestId());
        resourceResponse.setPosition(screenshot.getPosition());
        resourceResponse.setDevice(screenshot.getDevice());
        resourceResponse.setUrl(url);
        return resourceResponse;
    }

    //

    /**
     * Empty String to null in the form
     * @see <a href="https://stackoverflow.com/questions/2977649/is-there-an-easy-way-to-turn-empty-java-spring-form-input-into-null-strings">Stack Overflow</a>
     * @param binder WebDataBinder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }
}
