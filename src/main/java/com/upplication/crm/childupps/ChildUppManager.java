package com.upplication.crm.childupps;

import com.upplication.crm.repository.ChildUppRepository;
import es.upplication.entities.ChildUpp;
import es.upplication.entities.type.CustomerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.upplication.crm.repository.ChildUppSpecification.*;
import static org.springframework.data.jpa.domain.Specifications.where;

@Service
public class ChildUppManager {

    @Autowired
    ChildUppRepository childUppRepository;

    @Transactional(readOnly = true)
    public Page<App> getApps(Pageable pageable, String appName, int appId, CustomerType customerType, boolean deleted) {
        Page<ChildUpp> appsPage = childUppRepository.findAll(filters(appName, appId, customerType, deleted), pageable);
        List<ChildUpp> apps = appsPage.getContent();
        return new PageImpl<>(populate(apps), pageable, appsPage.getTotalElements());
    }

    private Specifications<ChildUpp> filters(String appName, int appId, CustomerType customerType, boolean deleted) {
        Specifications<ChildUpp> filters = null;
        if (appName != null && appName.trim().length() > 0) {
            filters = where(filterByName(appName));
        }

        if (appId > 0){
            if (filters != null){
                filters = filters.and(filterById(appId));
            } else {
                filters = where(filterById(appId));
            }
        }

        if (customerType != null){
            if (filters != null){
                filters = filters.and(filterByCustomerType(customerType));
            } else {
                filters = where(filterByCustomerType(customerType));
            }
        }

        if (!deleted) {
            if (filters != null){
                filters = filters.and(filterByNotDeleted());
            } else {
                filters = where(filterByNotDeleted());
            }
        }

        if (filters == null){
            filters = where(null);
        }

        return filters;
    }

    private List<App> populate(List<ChildUpp> apps) {
        List<App> result = new ArrayList<>();

        for (ChildUpp app : apps) {
            if (app.getFeatures() != null) {
                result.add(App.fromEntity(app));
            }
        }

        return result;
    }

}
