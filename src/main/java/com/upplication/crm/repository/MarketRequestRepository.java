package com.upplication.crm.repository;

import es.upplication.entities.PublishRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MarketRequestRepository extends JpaRepository<PublishRequest, Integer>, JpaSpecificationExecutor<PublishRequest> {
}
