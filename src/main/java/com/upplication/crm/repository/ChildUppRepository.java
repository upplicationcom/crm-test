package com.upplication.crm.repository;

import es.upplication.entities.ChildUpp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ChildUppRepository extends JpaRepository<ChildUpp, Integer>,
        JpaSpecificationExecutor<ChildUpp> {
    /**
     * Find ChildUpps without ChildUppRequest active in the month setted by the billDate parameter
     *
     *
     * @param billDate Date with the month to check. This date must start at day 1/month/year
     * @return List with ChildUpp, never null
     */
    @Query(value =
            "SELECT * FROM childupp c \n" +
            "   inner join administrator_has_childupp ahc on ahc.childupp_id = c.id\n" +
            "   inner join webuser owner on owner.id = ahc.admin_id\n " +
            "   inner join historic_features feature on feature.childuppId = c.id\n " +
            "   left join childupp_has_seller chs on chs.childupp_id = c.id \n" +
            "   left join seller s on s.id = chs.seller_id\n " +
            "WHERE \n" +
            "   feature.type = 'ACTIVE' AND " +
            "   owner.customerType = 'APLICATECA' AND " +
            "   feature.namePricing <> 'aplicateca.tasting' AND " +
            // not deleted or deleted this month and deleted this month but almost 30 days active or executive aplicateca
            "   (c.deleteDate IS NULL OR (c.deleteDate >= :bill_date AND (DATE_ADD(c.creationDate, INTERVAL 1 MONTH) <= c.deleteDate OR feature.namePricing = 'aplicateca.executive'))) AND " +
            // creation date hace mas de 30 días que la fecha de facturación o tiene el plan executive y se ha creado antes de la fecha de facturación
            "   ((DATE_ADD(c.creationDate, INTERVAL 1 MONTH) < DATE_ADD(:bill_date, INTERVAL 1 MONTH)) " +
            "   OR feature.namePricing = 'aplicateca.executive') AND c.creationDate < DATE_ADD(:bill_date, INTERVAL 1 MONTH)" +
            "   -- exclude from childupp_request\n" +
            "   and not exists (select * from childupp_request rq where rq.childupp = c.id)" +
            "group by c.id", nativeQuery = true)
    List<ChildUpp> childUppAplicateca(@Param("bill_date") Date billDate);

    @Query(value =
        "SELECT count(*) as count,  \n" +
        "CASE\n" +
        "WHEN pricing = '18' THEN 'basic'\n" +
        "WHEN pricing = '19' THEN 'prof'\n" +
        "WHEN pricing = '20' THEN 'premium'\n" +
        "WHEN pricing = '22' THEN 'executive'\n" +
        "END as plan\n" +
        "FROM childupp_request c inner join webuser w on w.id = c.web_user where type = 'ADD' and \n" +
        "DATE(created) = DATE(:day) and \n" +
        "w.customerType = 'APLICATECA' and\n" +
        "-- pricing = 21 is aplicateca.tasting (free)\n" +
        "pricing != 21 group by pricing", nativeQuery = true)
    List<Object[]> countAdditionsApplicatecaChildUppsForADay(@Param("day") Date day);

    @Query(value =
            "SELECT count(*) as count,  \n" +
            "CASE\n" +
            "WHEN pricing = '18' THEN 'basic'\n" +
            "WHEN pricing = '19' THEN 'prof'\n" +
            "WHEN pricing = '20' THEN 'premium'\n" +
            "WHEN pricing = '22' THEN 'executive'\n" +
            "END as plan\n" +
            "FROM childupp_request c inner join webuser w on w.id = c.web_user where type = 'REMOVE' and \n" +
            "DATE(created) = DATE(:day) and \n" +
            "w.customerType = 'APLICATECA' and\n" +
            "-- pricing = 21 is aplicateca.tasting (free)\n" +
            "pricing != 21 group by pricing", nativeQuery = true)
    List<Object[]> countDeletedAplicatecaChildUppsForADay(@Param("day") Date day);

    @Query(value =
            "SELECT count(*) as count, hf.namePricing as plan FROM \n" +
            "historic_features hf inner join childupp c on hf.childUppId = c.id inner join administrator_has_childupp ac on ac.childupp_id = c.id \n" +
            "inner join webuser wu on wu.id = ac.admin_id \n" +
            "WHERE hf.status = 'PAID' and wu.customerType = :customerType and wu.email not like 'newrelic_email%' and DATE(c.creationDate) = DATE(:day) GROUP BY hf.namePricing", nativeQuery = true)
    List<Object[]> countAdditionsChildUppsForADayAndCustomerType(@Param("day") Date day, @Param("customerType") String customerType);

    @Query(value =
            "SELECT count(*) as count, hf.namePricing as plan FROM \n" +
            "historic_features hf inner join childupp c on hf.childUppId = c.id inner join administrator_has_childupp ac on ac.childupp_id = c.id \n" +
            "inner join webuser wu on wu.id = ac.admin_id \n" +
            "WHERE hf.status = 'PAID' and wu.customerType = :customerType and wu.email not like 'newrelic_email%' and DATE(c.deleteDate) = DATE(:day) GROUP BY hf.namePricing", nativeQuery = true)
    List<Object[]> countDeletedChildUppsForADayAndCustomerType(@Param("day") Date day, @Param("customerType") String customerType);

    // FIXME use List<CustomerEmailInfoTO> instead, but there are parse troubles with that
    // At this moment Upplication company only emits invoice for Upplication customers
    @Query(value =
        "SELECT wu.id, po.amount, po.date, wu.identification \n" +
        "FROM webuser wu inner join administrator_has_childupp ac on wu.id = ac.admin_id inner join childupp c on ac.childupp_id = c.id inner join historic_features hf on c.id = hf.childUppId inner join payment_order po on po.feature = hf.id\n" +
        "WHERE po.type != 'NONE' and po.status = 'SUCCESS' and wu.customerType = 'UPPLICATION' and po.billing_date IS NOT NULL and po.billing_date >= :bill_date and po.billing_date < DATE_ADD(:bill_date, INTERVAL 1 MONTH) order by po.date" , nativeQuery = true)
    List<Object[]> getMonthlyCustomersInfo(@Param("bill_date") Date billDate);
}
