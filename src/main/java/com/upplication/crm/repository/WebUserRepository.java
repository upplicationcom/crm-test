package com.upplication.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import es.upplication.entities.WebUser;

@Repository
public interface WebUserRepository extends JpaRepository<WebUser, Integer>,
        JpaSpecificationExecutor<WebUser> {
}
