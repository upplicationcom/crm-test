package com.upplication.crm.util;

import org.im4java.core.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Implementation using ImageMagick library im4java.
 */
public class ImageMagickTransformer {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Indica que el tamaño de un parametro
     * geometry tiene que ser como mínimo como
     * se indica
     *
     * @see {http://www.imagemagick.org/script/command-line-processing.php#geometry}
     */
    private String geometryBigger = "^";
    /**
     * Indica que solo encoge el resize
     *
     * @see {http://www.imagemagick.org/script/command-line-processing.php#geometry}
     */
    private String geometryShrinks = ">";
    /**
     * Indica que el sitio de cortado
     * es el centro.
     */
    private String gravityCenter = "Center";
    /**
     * Indica que la imagen se debe guardar en
     * la salida estandar
     */
    private String writteStdout = ":-";
    /**
     * Indica la calidad que se quiere aplicar
     * a la imagen. 100% - 0%
     */
    private Double qualityImage = 100D;
    /**
     * String que representa el path donde se encuentra
     * ImageMagick
     */
    private String imageMagickPath;
    /**
     * String que representa el encode utilizado
     * para las imagenes
     */
    private String imageMagickEncoder = "PNG";

    public ImageMagickTransformer(String imageMagickPath, String quality) {
        // cargar del properties
        this.imageMagickPath = imageMagickPath;
        if (quality != null) {
            try {
                Double qualityDouble = Double.valueOf(quality);
                this.qualityImage = qualityDouble;
            } catch (NumberFormatException nfe) {
                log.warn("bad properties configured: " + quality);
            }
        }
    }

    /**
     * Transform the image filePath using ImageMagick to a exact x, y and density.
     *
     * @param filePath Path mandatory
     * @param fileToSave Path mandatory
     * @param x the width size in pixels
     * @param y the height size in pixels
     * @param density pp
     * @throws IOException If something goes wrong
     */
    public void transform(Path filePath, Path fileToSave, int x, int y, int density) throws IOException {
        log.debug("We are going to transform the image with density: " + density + " [x:" + x + "] [y:" + y + "]");
        transformExactFile(filePath, fileToSave, x, y, density);
    }

    private void transformExactFile(Path filePath, Path fileToSave, int x, int y, int density) throws IOException {
        // set up command
        ConvertCmd cmd = new ConvertCmd();
        cmd.setSearchPath(imageMagickPath);
        // create the operation, add images and operators/options
        IMOperation op = new IMOperation();
        // fichero original
        String filePath1frame = filePath.toAbsolutePath().toString() + "[0]"; //(solo el primer frame)
        op.addImage(filePath1frame);
        // especificamos que recortemos en el centro
        op.gravity(gravityCenter);
        // filtrado de caja
        op.filter("box");
        // redimensionar a un tamaño mínimo
        op.resize(x, y, geometryBigger);
        // recortar
        op.extent(x, y);
        // activar modo progresivo JPEG
        op.interlace("plane");
        // calidad de la compresiÃ³n imagen
        if (qualityImage > 0) {
            op.quality(qualityImage);
        }
        // densidad
        op.density(density);
        // guardamos en la ruta especificada
        op.addImage(fileToSave.toAbsolutePath().toString());

        if (log.isDebugEnabled()) {
            log.debug("ImageMagick Commands to execute: " + getCommandString(cmd, op));
        }
        // exec
        try {
            cmd.run(op);
        } catch (IOException | InterruptedException | IM4JavaException e) {
           throw new IOException("Unknown error trying to convert the image: " + filePath + " to: " + fileToSave + " with X: " + x + ", Y:" + y + " & density: " + density, e);
        }

    }

    private String getCommandString(ImageCommand cmd, IMOperation op) {
        if (cmd != null) {
            StringBuilder commandBuff = new StringBuilder();
            for (String commandStep : cmd.getCommand()) {
                commandBuff.append(commandStep + " ");
            }

            StringBuilder operationsBuff = new StringBuilder();
            for (String operationStep : op.getCmdArgs()) {
                operationsBuff.append(operationStep + " ");
            }

            return "[Commands: " + commandBuff + "] [Args: " + operationsBuff + "]";
        } else {
            return null;
        }
    }


    /**
     * ImageMagick path establecido por IoC Spring
     *
     * @param imageMagickPath String
     */
    public void setImageMagickPath(String imageMagickPath) {
        this.imageMagickPath = imageMagickPath;
    }

    /**
     * ImageMagick enconder establecido por IoC Spring
     *
     * @param imageMagickEncoder String
     */
    public void setImageMagickEncoder(String imageMagickEncoder) {
        this.imageMagickEncoder = imageMagickEncoder;
    }

    /**
     * quality image, establecido por IoC Spring
     *
     * @param qualityImage Double
     */
    public void setQualityImage(Double qualityImage) {
        this.qualityImage = qualityImage;
    }
}
