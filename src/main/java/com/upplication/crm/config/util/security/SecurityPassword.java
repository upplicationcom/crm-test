package com.upplication.crm.config.util.security;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.BasicTextEncryptor;
import org.jasypt.util.text.TextEncryptor;
import org.springframework.stereotype.Component;

@Component
public class SecurityPassword {

    /**
     * Decrypt the external password received and return decrypted value
     *
     * @param password valid encrypt password
     * @return decrypted password or <code>null</code> if is invalid
     */
    public static String decrypt(String password) {
        try {
            return getTextEncryptor().decrypt(password);
        } catch (EncryptionOperationNotPossibleException e) {
            // invalid token
            return null;
        }
    }

    /**
     * Encrypt the external password received and return encrypted value
     *
     * @param password valid plain password
     * @return encrypted password or <code>null</code> if is invalid
     */
    public static String encrypt(String password) {
        try {
            return getTextEncryptor().encrypt(password);
        } catch (EncryptionOperationNotPossibleException e) {
            // invalid token
            return null;
        }
    }

    /**
     * Get encryptor
     *
     * @return TextEncryptor
     */
    private static TextEncryptor getTextEncryptor() {
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword("UpPlIc4t10n10");
        return textEncryptor;
    }

}
