package com.upplication.crm.config.amqp;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class ItunesStatusQueueConfig {

    // Params for Itunes Status Environment
    @Autowired
    private Environment environment;

    @Bean
    public TopicExchange itunesStatusExchange() {
        return new TopicExchange(environment.getProperty("rabbitmq.itunes.publishStatus.exchange"));
    }

    @Bean
    public Queue itunesStatusQueue() {
        return new Queue(environment.getProperty("rabbitmq.itunes.publishStatus.queue"), true);
    }

    @Bean
    public Binding itunesStatusDeclareBinding() {
        return BindingBuilder.bind(itunesStatusQueue()).to(itunesStatusExchange()).with(environment.getProperty("rabbitmq.itunes.publishStatus.routingKey"));
    }

}
