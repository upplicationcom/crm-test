package com.upplication.crm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable()
                .authorizeRequests()
                    .antMatchers("/", "/resources/**", "/signin", "/signup").permitAll()
                    .anyRequest().authenticated()
                .and()
                .formLogin()
                    .failureUrl("/signin?error=1")
					.loginPage("/signin")
					.permitAll()
					.and()
                .logout()
                    .logoutUrl("/logout").permitAll();
    }

    // cretae in memory user
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin-crm@upplication.io").password("FenixCRM10").roles("admin");
    }
}