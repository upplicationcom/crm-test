package com.upplication.crm.webusers;

import com.upplication.crm.support.web.PageWrapper;
import es.upplication.entities.type.CustomerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class WebUsersController {

    @Autowired
    private WebUsersManager webUsersManager;

    private final static String WEBUSERS_VIEW = "webusers/index";

    @RequestMapping(value = "webusers", method = RequestMethod.GET)
    public String requests(Model model,
                           @PageableDefault(sort = {"registrationDate"}, direction = Sort.Direction.DESC, value = 20) Pageable pageable,
                           @RequestParam(value = "email", required = false) String email,
                           @RequestParam(value = "customerId", required = false) String customerId,
                           @RequestParam(value = "deleted", required = false, defaultValue = "false") Boolean deleted,
                           @RequestParam(value = "childUppId", required = false) Integer childUppId,
                           @RequestParam(value = "customerType", required = false, defaultValue = "UPPLICATION") CustomerType customerType) {

        PageWrapper<WebUser> page = new PageWrapper<>(webUsersManager.getWebUsers(pageable, email, customerId, customerType, childUppId, deleted), "webusers");

        model.addAttribute("page", page);
        model.addAttribute("email", email);
        model.addAttribute("childUppId", childUppId);
        model.addAttribute("deleted", deleted);
        model.addAttribute("customerId", customerId);
        model.addAttribute("customerType", customerType);

        return WEBUSERS_VIEW;
    }

    @RequestMapping(value = "webusers/{idWebUser}/childupps", method = RequestMethod.GET)
    @ResponseBody
    public ChildUppInfo publish(@PathVariable("idWebUser") int idWebUser) {
        return webUsersManager.getChildUppInfo(idWebUser);
    }

}
