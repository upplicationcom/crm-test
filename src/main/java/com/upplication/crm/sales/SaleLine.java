package com.upplication.crm.sales;

import es.upplication.entities.PaymentOrderType;

import java.util.Date;

public class SaleLine {

    private Date day;
    private Date start;
    private Date end;
    private String userName;
    private String childUppName;
    private int childUppId;
    private double amount;
    private String billNumber;
    private PaymentOrderType type;
    private String billUrlOds;
    private String billUrlPdf;

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public PaymentOrderType getType() {
        return type;
    }

    public void setType(PaymentOrderType type) {
        this.type = type;
    }

    public int getChildUppId() {
        return childUppId;
    }

    public void setChildUppId(int childUppId) {
        this.childUppId = childUppId;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getBillUrlOds() {
        return billUrlOds;
    }

    public void setBillUrlOds(String billUrlOds) {
        this.billUrlOds = billUrlOds;
    }

    public String getBillUrlPdf() {
        return billUrlPdf;
    }

    public void setBillUrlPdf(String billUrlPdf) {
        this.billUrlPdf = billUrlPdf;
    }

    public String getChildUppName() {
        return childUppName;
    }

    public void setChildUppName(String childUppName) {
        this.childUppName = childUppName;
    }
}
