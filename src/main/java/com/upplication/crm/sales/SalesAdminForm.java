package com.upplication.crm.sales;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import java.util.Date;

public class SalesAdminForm {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date start;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date end;

    private Integer childUppId;

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    @Digits(integer = 15, fraction = 4)
    public Integer getChildUppId() {
        return childUppId;
    }

    public void setChildUppId(Integer childUppId) {
        this.childUppId = childUppId;
    }

}
